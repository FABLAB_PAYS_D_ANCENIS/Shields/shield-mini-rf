#include <SPI.h>
#include <SoftwareSerial.h>
#include "CSystemMiniRf.h"

// Classes
CRadioFreq     *RadioFreq;
CSystemMiniRf  *System;

// Variables
uint32 u32Tempo;

/*****************************************************************************
 * Function    : setup                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void setup()
{
    // Serial
    //Serial.begin(9600);

    // Instanciate classes
    System      = new CSystemLcdANum();
    RadioFreq   = new CRadioFreq();

    System->configure();
    System->disableErrors();

    // Init variables
    u32Tempo   = millis();
}

/*****************************************************************************
 * Function    : loop                                                        *
 *...........................................................................*
 * Description : The loop function runs over and over again forever          *
 *****************************************************************************/
void loop()
{
    int32  s32Tmp;
    uint16 u16Tmp;
    uint32 u32Tmp;

    System->check();
            
	if((millis() - u32Tempo) > 2000)
	{


		u32Tempo = millis();
	}   
}
