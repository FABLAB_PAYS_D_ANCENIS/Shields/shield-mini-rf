#ifndef CSystemMiniRf_H
#define CSystemMiniRf_H


extern char m_u8PcbRevision;

// Pin definition
extern int cPinLuminosity;
extern int cPinTemperature;
extern int cPinPotentiometer;
extern int cPinDebugLed;

extern int cPinBuzzer;

extern int cPinSimpleLedRed;
extern int cPinSimpleLedOrange;
extern int cPinSimpleLedGreen;

extern int cPinHallEffect;
extern int cPinIrTransmit;
extern int cPinIrReceive;
extern int cPinButtonUp;
extern int cPinButtonLeft;
extern int cPinButtonRightDown;

extern int cPin7SegClock;
extern int cPin7SegDio;

extern int cPinRfChipEnable;
extern int cPinRfChipSelect;
extern int cPinRfMiso;
extern int cPinRfMosi;
extern int cPinRfClock;
extern int cPinRfIrq;

extern int cPinInclinometer;
extern int cPinRgbLed;
extern int cPinExtension;

#define cNUMRgbLeb            1

// Types definition
typedef signed char         int8;
typedef unsigned char       uint8;
typedef short               int16;
typedef unsigned short      uint16;
typedef long                int32;
typedef unsigned long       uint32;
typedef unsigned long long  uint64;



class CSystemMiniRf
{
public:
    CSystemMiniRf();
    void configure(void);
    void check(void);
    void disableErrors(void);
	void error(uint8 u8Err);

private:
    bool bErrorDisabled;
};

#endif // CSystemMiniRf_H
