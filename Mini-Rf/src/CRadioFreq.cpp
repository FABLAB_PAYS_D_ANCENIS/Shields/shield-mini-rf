#include "CRadioFreq.h"

char m_bRadioFreqUSed = false;

/*****************************************************************************
 * Function    : CButton                                                     *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CRadioFreq::CRadioFreq()
{
  // Init class
    _radio = new NRFLite();

  // Set initialize flag to false
  bIsInitialized  = false;
  s32CodeReceived = -1;
  u8AddrReceived  = -1;
  m_bRadioFreqUSed  = true;
}

/*****************************************************************************
 * Function    : init                                                        *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRadioFreq::init(uint8 u8Id)
{
  // Try to init RF
    if(!_radio->init(u8Id, cPinRfChipEnable, cPinRfChipSelect, NRFLite::BITRATE250KBPS, 104))
    {
        Serial.println("Cannot communicate with radio");
    }
    else
    {
        Serial.println("radio Ok");

       // Initialize flag to true
       bIsInitialized = true;
    }

    // Store emitter address
    u8AddrEmitter = u8Id;
}

/*****************************************************************************
 * Function    : init                                                        *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRadioFreq::destAddress(uint8 u8Id)
{
    // Store emitter address
    u8AddrDest = u8Id;
}

/*****************************************************************************
 * Function    : stop                                                        *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRadioFreq::stop(void)
{
    // Check init status
    if(bIsInitialized == true)
   {
      return _radio->powerDown();
   }
}

/*****************************************************************************
 * Function    : isCodeReceived                                              *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CRadioFreq::isCodeReceived(void)
{
  // Check if initialization already done
  if(bIsInitialized == false)
  {
    // Force init to adress 255
    init(255);
  }

  // Check init status
  if(bIsInitialized == true)
  {
      // If received
      if(_radio->hasData())
      {
          // Read payload
          _radio->readData(&_radioData);

          // Store it
          s32CodeReceived = _radioData.s32Data;
          u8AddrReceived  = _radioData.u8Emitter;
          u16TypeReceived = _radioData.u16Identifier;

          return true;
      }
      else
      {
          return false;
      }
  }
  else
  {
      return false;
  }
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
int32 CRadioFreq::receiveCode(void)
{
    // Check if initialization already done
    isCodeReceived();

    // Check init status
    if(bIsInitialized == true)
    {
        // Read if data present
        return s32CodeReceived;
    }

    return -1;
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
int16 CRadioFreq::receiveAddr(void)
{
    // Check if initialization already done
    isCodeReceived();

    // Check init status
    if(bIsInitialized == true)
    {
        // Read if data present
        return u8AddrReceived;
    }

    return -1;
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
int32 CRadioFreq::receiveType(void)
{
    // Check if initialization already done
    isCodeReceived();

    // Check init status
    if(bIsInitialized == true)
    {
        // Read if data present
        return u16TypeReceived;
    }

    return -1;
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRadioFreq::send (int32 s32Data, uint16 u16Id)
{
    // Check if initialization already done
    if(bIsInitialized == false)
    {
        // Force init to adress 255
        init(255);
    }

    // Check init status
    if(bIsInitialized == true)
    {
        // Build payload
        _radioData.s32Data   = s32Data;
        _radioData.u16Identifier = u16Id;
        _radioData.u8Emitter = u8AddrEmitter;

        // Send it
        if (_radio->send(u8AddrDest, &_radioData, sizeof(_radioData)))
        {
            Serial.println("...Success");
        }
        else
        {
            Serial.println("...Failed");
        }
    }
}
