#include "CDebugLed.h"

char m_bSimpleLedUSed = false;

/*****************************************************************************
 * Function    : CDebugLed                                                  *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CDebugLed::CDebugLed()
{
    // Setup pin
    pinMode(cPinDebugLed,    OUTPUT);

    m_bSimpleLedUSed = true;
}

/*****************************************************************************
 * Function    : CDebugLed                                                  *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CDebugLed::init(void)
{
    // Setup pin
    pinMode(cPinDebugLed,    OUTPUT);
}

/*****************************************************************************
 * Function    : switchOnLed                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CDebugLed::switchOnLed(uint8 u8Led)
{

        m_bSimpleLedRedUSed = true;
        digitalWrite(cPinDebugLed, true);

}

/*****************************************************************************
 * Function    : switchOffLed                                                *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CDebugLed::switchOffLed(uint8 u8Led)
{

        m_bSimpleLedRedUSed = true;
        digitalWrite(cPinDebugLed, false);

}

/*****************************************************************************
 * Function    : invertLed                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CDebugLed::invertLed(uint8 u8Led)
{

        m_bSimpleLedRedUSed = true;
        if(digitalRead(cPinDebugLed))
            digitalWrite(cPinDebugLed, false);
        else
            digitalWrite(cPinDebugLed, true);

}
