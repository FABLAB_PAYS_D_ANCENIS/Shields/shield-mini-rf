#include "CSystemMiniRf.h"

char m_u8PcbRevision;

int cPinLuminosity = A2;
int cPinTemperature = A4;

int cPinDebugLed = 4;

int cPinAnaInput = A1;

int cPinRfChipEnable = 8;
int cPinRfChipSelect = 10;
int cPinRfMiso = 12;
int cPinRfMosi = 11;
int cPinRfClock = 13;
int cPinRfIrq = 2;


/*****************************************************************************
 * Function    : CSystemMiniRf                                              *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CSystemMiniRf::CSystemMiniRf()
{
    // Init pcb rev to 0 = unknown
    m_u8PcbRevision = 0;
    bErrorDisabled  = false;

    // Detect Pcb
    configure();
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemMiniRf::configure(void)
{
   m_u8PcbRevision = 1;
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemMiniRf::disableErrors(void)
{
    bErrorDisabled = true;
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemMiniRf::check(void)
{
    // Check if error is disabled
    if(bErrorDisabled == true)
        return;

    // On Rev 1 pcb
    if(m_u8PcbRevision == 1)
    {

    }
    // On Rev 2 pcb
    else if(m_u8PcbRevision == 2)
    {

    }
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystemMiniRf::error(uint8 u8Err)
{
    if(u8Err == 1)
    {
        
    }
    else if(u8Err == 2)
    {
        
    }

    // Stop processing
    while(1){}
}
