#ifndef CDEBUGLED_H
#define CDEBUGLED_H

#include "ShieldMiniRf.h"

extern char m_bSimpleLedUSed;
extern char m_bSimpleLedGreenUSed;
extern char m_bSimpleLedOrangeUSed;
extern char m_bSimpleLedRedUSed;


class CDebugLed
{
public:
    CDebugLed();
    void init(void);
    void switchOnLed(uint8 u8Led);
    void switchOffLed(uint8 u8Led);
    void invertLed(uint8 u8Led);

private:


};

#endif // CDEBUGLED_H
